# CS350 Container
This is CS350 Docker container repository. It includes the CS350 Docker container and the tools to build, run, debug and test OS/161 and Linux based programming assignments for CS350. This repository currently expects that you are running in a UNIX or UNIX-like environment that supports docker, such as, MacOS, Linux or Windows WSL. 

If you have a Windows system without WSL, then start [here](https://git.uwaterloo.ca/krhancoc/cs350-container/-/edit/master/README.md#windows).

If you do not have access to a UNIX or UNIX-like environment then you can use the 
[older instructions](https://student.cs.uwaterloo.ca/~cs350/common/Install161.html) to build, run, debug and test in University of Waterloo's linux student environment.

## Windows Option A
This is an emulated environment and allows for bash like commands, but you are running windows underneath. You will need to use
Git Bash for all commands stated in this README and to run any of the provided scripts.
* Install [Git for Windows](https://gitforwindows.org/)
* Install [docker](https://docs.docker.com/desktop/windows/install) for Windows.

## Windows Option B
This setup will let you have a full linux environment on your windows machine, WSL2 is just a linux VM running on your computer!
* Install [WSL](https://docs.microsoft.com/en-us/windows/wsl/install).
* Install [docker](https://docs.docker.com/desktop/windows/install) for WSL.


## UNIX and UNIX-like Environments - Prerequiste to install Docker
* Install [docker](https://docs.docker.com/engine/install/)

## Prerequiste - repository for OS/161 source code 
* This repository assumes that you have a copy of the OS/161 source code on the local machine.
* You **should** have a remote repository to manage your OS/161 source code and move between the local and linux.student environments. 

* If you do not currently have a working copy of OS/161, you can retrieve your own by copying the
`os161.tar.gz` file within `os161-container/dependencies` directory from the cs350-container repository.
```
cp os161-container/dependencies/os161.tar.gz ~/
cd ~/
tar xzvf os161.tar.gz
```
You should now have a folder called `os161-1.99` in your home directory. Make this a git repository!

## Prerequiste complete - install CS350-container
Once the above requirements are met, make sure docker desktop is running (if on WSL or MacOS), then you can clone this repository and install the cs350-container by typing:
```
sudo ./install.sh
```
In a terminal from within the project directory. This script will pull the assignments reposiroty and build it into a local copy of the os161-runner image.

## Running OS/161 in the cs350-container
To start using the container you use the run.sh script, which takes one argument -- the directory of your working code repository.

```
sudo ./run.sh ~/os161-1.99
```
Once executed you will be within the context of the container itself (its like your own mini environment), there are two functions (found at /helpers.sh within the container), `build_kernel ASST#`, and `test_kernel ASST#`. Where the `#` represents the
assignment number. You can run these commands from the shell container. These functions are preloaded in your shell. For example, to build and run ASST0: 
```
# build_kernel ASST0
# test_kernel ASST0
```

To fully evaluate youre kernel with the automated testing scripts you may use
```
# ./run_assignment.sh ASST#
```
This will run `build_kernel` and `test_kernel` as well as evaluate the output produced by `test_kernel` (The raw output for build and test kernel can be found in /logs).


If you would like to run the kernel normally (no scripts), once built, you can run the kernel
```
# cd /os-compile
# sys161 kernel
```

There also exists a `clean_kernel ASST#` command which will try and clean up old builds, this may be necessary if running into issues. If this 
fails that is okay (means there was no build to begin with), just run `build_kernel ASST#` as normal.

## Running GDB
* Make sure you have built the kernel using `build_kernel ASST0`.
* To run GDB, open a terminal with the cs350-container running, and enter the following commands:
```
# cd /os-compile/
# sys161 -w kernel
```

* Now in another terminal, move to the project directory and use the `./connect.sh` script to connect to the previosuly running container:
```
# cd os-compile/
# cs350-gdb kernel

GNU gdb 6.6
Copyright (C) 2006 Free Software Foundation, Inc.
...
(gdb) dir /os161-1.99/kern/compile/ASST0
(gdb) target remote unix:.sockets/gdb
...
```
You will need to use the command `dir /kernel/kern/compile/ASST#` where `#` is the assignment number. This will make sure to load the symbols
of the kernel. Symbols are the names we have given functions and variables. This allows gdb to correctly convey what you are looking at, since the kernel has been compiled WITH these symbols, otherwise gdb will show just bare assembly!

Note: If run.sh fails it means there is still a os161-runner container running. Use `docker kill os161` to kill it.

## Building and installing OS/161 userspace programs
User-level programs are built and installed from the directory /os161-1.99/. 
To build and install all of the user-level programs, in the container

You can type
```
cd /os161-1.99/
bmake
bmake install
```
User-level programs are installed under $OS161TOP/root/ in the bin, sbin, testbin and uw-testbin directories. This is done automatically
by the build_kernel command.

## Running Linux userspace assignments
To run the Linux userspace programs in the container, that are part of the progrmaming assignment, you must start the container using the `run.sh` script, which takes one argument -- the directory that holds your code for that userspace programming assignment, that is, the `c` files for the programming assignments. For example,  

```
sudo ./run.sh ~/cs350-teststudent/a0
```
Once you are inside the container, you can run
```
./run_assignment.sh ASSTUSER#`
```
 where `#` represents the assignment number. 

It is important to note that: when mounting other directories that are not OS/161, the `build_kernel` and `test_kernel` functionality will not work.

## [Layout](Layout)
There are some noted directories within the container
* `/kernel` holds YOUR assignment code
* `/os161-1.99` holds unchanged OS/161 kernel code
* `/os-compile` holds the compiled version of the code
* `/assignments` holds testing and evaluation scripts used for each assignment

Within the `/assignments` directory there are subdirectories for each assignment, which you can review:
* the `public` file - shows the test cases we pass and use,
* the `run.sh` file - how we run and build your assignment,
* the  `verify.py` script - how we evaluate the output of your assignment.

If the assignments repo has changed, either due to assignments being changed or bugs found (found at `os161-container/assignments`) you will need to just run `sudo ./install.sh` again.


## Common Issues

**Commands do not work and also there is randomly ^M, strings within the output:**
This is due to windows placing return characters in the strings that get
passed to the scripts. Please use git bash or a terminal that does UNIX
string passing.

**Cannot figure out the host**
This occurs within newer level Macs that use Apple Silicon, docker
currently requires x86 architectures to work, you'll need to use the
student environment.

