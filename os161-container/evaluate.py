#!/usr/bin/env python3
""" 
Evalaute

This script is expected to be run within the context of the os161 docker container

Expected directories are:
/logs
"""

import argparse
import os
import importlib.util
from pprint import pprint as pprint

from prettytable import PrettyTable

HEADER = ["Section Name", "Marks Recieved", "Out Of", "Comments"]


class Report:
    def __init__(self):
        self._marks = []

    def add(self, name, marks, out_of, comments):
        self._marks.append([name, marks, out_of, comments])

    def __str__(self):
        total = 0
        table = PrettyTable(HEADER)
        recieved = 0
        for x in self._marks:
            total += x[2]
            recieved += x[1]
            table.add_row(x)
        total = "Total Marks = {} / {}".format(recieved, total)
        tb = str(table) + "\n" + total

        return tb

    def to_csv(self, file):
        with open(file, "w") as f:
            header = ",".join(HEADER) + "\n"
            f.write(header)
            for l in self._marks:
                line = ",".join([str(m) for m in l]) + "\n"
                f.write(line)


class Test:
    def __init__(self, log):
        log = [l for l in log if len(l) > 0]
        self.name = log[0].split("=")[1].strip("<").strip(">")
        # Remove the TEST=#### line
        self._log = "\n".join(log[1:])

    def results(self):
        return self._log


def check_all(strs, res):
    return any([f in res for f in strs])


class Namespace:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


def main(args):
    tests = []
    with open(args.log, "r") as f:
        log = f.read()
        log.strip()
        # Remove one cause topline is a split so would get empty line before
        tmp = log.split(">SPLIT<")[1:]
        for t in tmp:
            t.strip()
            tests.append(Test(t.split("\n")))
    # We load in the verify.py from the assignment folder
    spec = importlib.util.spec_from_file_location(
        "verify", os.path.join(args.vdir, "verify.py")
    )
    verify = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(verify)

    helpers = Namespace(check_all=check_all)

    # Generate the report for the student
    report = Report()
    for eval_func, out_of in verify.RUBRIC.items():
        name, mark, comments = eval_func(tests, helpers)
        report.add(name, mark, out_of, comments)
    print(str(report))
    report.to_csv("/logs/report.csv")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="Evaluate script for CS350 test logs")
    parser.add_argument("log", help="Log file of the test")
    parser.add_argument(
        "vdir", help="Path to test Directory holding tests and verify.py"
    )
    args = parser.parse_args()
    main(args)
